# AMQ 6.3 Example

## Create Template
    oc create -f amq-template.yml

## Import AMQ Image
    oc import-image amq63-openshift --from=registry.redhat.io/jboss-amq-6/amq63-openshift --confirm

## Create AMQ
    oc new-app --template=amq63 --param=GIT_URI=https://gitlab.com/openshift-samples/amq63.git

## Clean Everything
    oc delete all -l template=amq63 && oc delete pvc broker-amq-claim
